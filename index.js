const express = require("express");
// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate
const mongoose = require("mongoose");
const port = 3001;

// [SECTION]  MongoDB Connection
	// Syntax:
		/*
			mongoose.connect("<MongoDB Atlas Connection String>"),
				{
					// Allows us to avoid any current and future errors while connecting to mongoDB
					useNewUrlParser: true,
					useUnifiedTopology: true
				}
		*/
const app = express();

	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.zxhvj7d.mongodb.net/batch203_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection;

	db.on("error", console.error.bind(console,"connection error"));

	db.once("open", () => console.log("We're connected to the cloud database."));

	// [SECTION] Mongoose Schemas
		// Schemas determine the structure of the documents to be written in the database
		// Schemas act as blueprints to our data
		// Syntax:
			/*
				const schemaName = new mongoose.Schema({<keyvalue:pair>})
			*/
		// name & status
		// "required" is used to specify that a field must not be empty.
		// 
		const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, "Task name is required"]
			},
			status: {
				type: String,
				default: "pending"
			}
		})

		const userSchema = new mongoose.Schema({
			username: {
				type: String,
				required: [true, "User name is required"]
			},
			password: {
				type: String,
				required: [true, "Password is required"]
			}
		})

		// [SECTION] Models
			// Uses schema and use it to create/instantiate documents/object that follows our schema structure.
			// The variable/object that will be created can be used to run commandes for interacting with our database.
			// Syntax: const VariableName = mongoose.model("collectionName",schemaName);

			const Task = mongoose.model("Task", taskSchema);

			const User = mongoose.model("User", userSchema);

// middlewares
app.use(express.json()); // Allows app to read json data
app.use(express.urlencoded({extended:true})); // Allows your app to read data from forms.

	app.post("/tasks",(req,res)=>{
		Task.findOne({name:req.body.name}, (err,result) =>{
			if(result !== null && result.name == req.body.name){
				return res.send("Duplicate task found!");
			}
			else {
				// "newTask" was created/instantiated from the Mongoose schema and will gain access to ".save" method
				let newTask = new Task({
					name:req.body.name
				});

				newTask.save((saveErr, savedTask) => {
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return res.status(201).send("New Task created!");
					}
				})
			}
		})
	});

	app.get("/tasks", (req,res) => {
		Task.find({}, (err,result) =>{
			if(err){
				return console.log(err);
			}
			else {
				return res.status(200).send({
					data: result
				})
			}
		})
	})

	// ACTIVITY

	app.post("/signup", (req, res)=>{
		console.log(req.body.name);
		
		User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
			console.log(result);

			if(result !== null && result.username === req.body.username){
				return res.send("Duplicate user found!");
			}
			else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return res.status(201).send("New user registered");
					}
				})
			}
		})
	});

	app.get("/signup", (req,res) =>{
		User.find({}, (err, result) =>{
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).send({
					Users: result
				})
			}
		})
	})

app.listen(port, () => console.log(`Server running at port: ${port}`)); 